<?php
// $Id$

/**
 * @file
 *
 *   These are the backend commands that actually accomplish adding a second
 *   user, adds the drush command to notify them of it, assigns them a role and
 *   updates the login reset link.
 */

/**
 * Implementation of drush_hook_pre_hosting_task().
 *
 * Overrides the user that'll be used during install in create_admin_user()
 */
function drush_hosting_profile_roles_pre_hosting_task() {
  $task =& drush_get_context('HOSTING_TASK');

  if ($task->ref->type == 'site' && $task->task_type == 'install') {
    if ($uid1 = db_result(db_query('SELECT {uid} FROM {hosting_profile_roles_uid1} WHERE pl_nid=%d AND pr_nid=%d', $task->ref->platform, $task->ref->profile))) {
      $task->uid = user_load($uid1)->uid;
    }
    else {
      // If there's no platform-specific uid1, use default one if it exists.
      if ($uid1 = variable_get('hosting_profile_roles_default_uid1', FALSE)) {
        $task->uid = user_load($uid1)->uid;
      }
    }
  }
}

/*
 * Implementation of hook_post_hosting_TASK_TYPE_task().
 *
 * Runs the backend commands to create a second user, send a notification,
 * update the login link, and assign a role.
 */
function hosting_profile_roles_post_hosting_install_task($task, $data) {
  $role = db_result(db_query('SELECT {role} FROM {hosting_profile_roles_roles} WHERE pl_nid=%d AND pr_nid=%d', $task->ref->platform, $task->ref->profile));
  $uid1 = db_result(db_query('SELECT {uid} FROM {hosting_profile_roles_uid1} WHERE pl_nid=%d AND pr_nid=%d', $task->ref->platform, $task->ref->profile));

  // If not database results for this platform, use the default uid1
  if ($uid1 === FALSE) {
    $uid1 = variable_get('hosting_profile_roles_default_uid1', FALSE);
  }

  $client_name = $task->ref->name;
  $target = $task->ref->hosting_name;
  if ($uid1 != FALSE) {

    // if http://drupal.org/node/1116414 gets committed, create new branch and use drush notify
    $results['user-create'] = provision_backend_invoke($target, 'user-create ' . $client_name . ' --mail=' . user_load(array('name' => $client_name))->mail);
    drush_log(dt('User created:') . $results['user-create']['output']);

    $results['user-login'] = provision_backend_invoke($target, 'user-login ' . $client_name);
    drush_log(dt('Client user login link: ') . $results['user-login']['output']);

    // Set the site login link to the one we just created; ref.: hosting_site_post_hosting_login_reset_task()
    $cache = array(
      'expire' => strtotime("+24 hours"),
      'link' => $results['user-login']['output'],
    );
    cache_set('hosting:site:' . $task->ref->nid . ':login_link', $cache, 'cache', $cache['expire'] );
    $message = "status_activated";
    // before sending out the notification it may be nice to set the site_mail of the
    // newly provisioned site to the site_mail of hostmaster --better than admin@example.com anyways! 
    $result ['sitemail'] = provision_backend_invoke ($target, "eval \"variable_set('site_mail', '".variable_get('site_mail', 'admin@example.com')."')\"");
    // we are making the assumption that the newly created user is always UID 2
    // I guess this is always valid?
    $user_info = provision_backend_invoke($target, "user-information --full $client_name");
    $tmp_array = explode("\n", $user_info['output']);
    $tmp_array = explode(' ', $tmp_array[0]);
    $new_user_uid = intval($tmp_array[1]);

    drush_log('Parsed uid of new user of new site: ' . $new_user_uid);

    $result ['notify'] = provision_backend_invoke($target, "eval  '_user_mail_notify(\"".$message."\", user_load($new_user_uid));'");
    drush_log("Notified user $new_user_uid ($client_name)");
  }
  else {
    // we may want to do this for block-placement, etc.
    $result = provision_backend_invoke($target, 'eval "print(user_load(1)->name)"');
    $client_name = $result['output'];
  }

  if ($role != FALSE) {
    // TODO: support adding multiple roles
    // we can finally check that the role actually exists on this site
    $result = provision_backend_invoke($target, 'eval "print(serialize(user_roles()))"');
    $site_roles = unserialize($result['output']);
    if (in_array($role, $site_roles)) {
      // we need to avoid a role name like ";rm -rf /", which is a valid role name in D6, btw
      $role = drush_escapeshellarg($role);
      provision_backend_invoke($target, 'user-add-role ' . $role . ' --name=' . $client_name);
    }
    else {
      drush_set_error(DRUSH_APPLICATION_ERROR, dt('No role of <em>' . $role . '</em> is available on this platform'));
    }
  }
}
