Hosting Profile Roles
=====================

Hosting Profile Roles is a module that extends Aegir to enable more
control over what role(s) a client is assigned during site install. Since, by
default, Aegir client's are given UID1 during site installs, this module also
allows UID1 to be assigned to another user. This allows tighter control of the
client's user experience, and thus support for Software-as-a-Service (SaaS)
type business models.

Features
--------

Additions to the front-end:

- adds a "Roles" tab to the platform page that includes (for each profile
  available on the platform):
    - a field for an Aegir user that will be assigned UID1 for sites created
     using that profile
    - a field for the name of the role to assign site creators

When a site is created on a such a platform:

- UID1 on a created site is replaced (in the site context) by the Aegir user
  specified on the platform's Roles tab for the relevant profile
- after the site is installed, a second user is created based on site's creator
- that user is assigned the role specified on the platform's Roles tab for the
  relevant profile
- the "Login" link is updated to point to the second user's account

A Note on Security
------------------

This module rewrites the site login link to reset the client user's password,
rather than UID's. However, it currently only does this on site install, and so
running a 'login reset' task may present the client with a link to reset UID1's
password.

By default, client users don't have permission to create 'login reset' tasks
themselves. However, extending such permission, or an inadvertant reset by an
Aegir administrator, could give clients access to UID1 on their sites.

For more detail, see: http://drupal.org/node/1220062
