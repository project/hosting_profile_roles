<?php

/**
 * @file
 * Admin page for the hosting_profile_roles module.
 */

function hosting_profile_roles_admin_settings_form() {
  // Form for global settings. Currently only default uid.
  
  $account = user_load(variable_get('hosting_profile_roles_default_uid1', ''));
  
  $default_uid1 = $account === NULL ? '' : $account->name;

  $form = array();

  $form['default_uid1'] = array(
    '#type' => 'textfield',
    '#title' => 'Default root user',
    '#default_value' => $default_uid1, 
    '#autocomplete_path' => 'user/autocomplete',
    '#description' => "Default root user (uid1). Will be used if the platform doesn't specify a root user.",
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

function hosting_profile_roles_admin_settings_form_validate($form, &$form_state) {
  if (user_load(array('name' => $form_state['values']['default_uid1'])) == 0) {
    form_set_error('default_uid1', '<em>' . $form_state['values']['default_uid1'] . '</em> ' . t('is not a valid user.'));
  }
}

function hosting_profile_roles_admin_settings_form_submit($form, &$form_state) {
  $account = user_load(array('name' => $form_state['values']['default_uid1']));

  if ($account === NULL) {
    variable_del('hosting_profile_roles_default_uid1');
  }
  else {
    variable_set('hosting_profile_roles_default_uid1', $account->uid);
  }

  drupal_set_message(t('The configuration options have been saved.'));
}
